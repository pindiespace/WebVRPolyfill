"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * This polyfill will create the WebVR API on desktop and mobile. The Desktop version does nothing except creating WebVR object in the global scope.
 * The mobile version will uses the orientation events to provide a baasic head tracking.
 *
 * The API and docs come from Mozilla Developer Docs https://developer.mozilla.org/en-US/docs/Web/API/WebVR_API
 *
 * Licence: MIT
 * Author: Yannick Comte
 * Initial release: 01-20-2016
 */
(function () {
	// Generates an UUID.
	var generateUUID = function generateUUID() {
		return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
	};

	/**
  * Gets available VR devices.
  * @method getVRDevices
  * @return {Promise} Returns a promise that resolves to an array of objects representing any VR devices that are connected to the computer and compatible with the browser.
  */
	var getVRDevices = function getVRDevices() {
		return new Promise(function (resolve, reject) {
			// Mobile (If any one has a better method...)
			if (navigator.userAgent.match(/android|iphone|windows phone|mobile/i)) {
				var guid = generateUUID();
				var devices = [];
				devices.push(new MobileHMDVRDevice(guid));
				devices.push(new MobilePositionSensorVRDevice(guid));
				resolve(devices);
			}
			// Firefox
			else if (navigator.mozGetVRDevices) {
					navigator.mozGetVRDevices(resolve);
				}
				// Other (Chrome has already the correct implementation.)
				else {
						reject();
					}
		});
	};

	var __applyPolyfill = function __applyPolyfill() {
		/**
   * Contains the raw definition for the degree value properties required to define a field of view. Inherited by VRFieldOfView.
   * @class VRFieldOfViewReadOnly
   */

		var VRFieldOfViewReadOnly = function VRFieldOfViewReadOnly() {
			var up = arguments.length <= 0 || arguments[0] === undefined ? 0 : arguments[0];
			var right = arguments.length <= 1 || arguments[1] === undefined ? 0 : arguments[1];
			var down = arguments.length <= 2 || arguments[2] === undefined ? 0 : arguments[2];
			var left = arguments.length <= 3 || arguments[3] === undefined ? 0 : arguments[3];

			_classCallCheck(this, VRFieldOfViewReadOnly);

			this.upDegrees = up;
			this.rightDegrees = right;
			this.downDegrees = down;
			this.leftDegrees = left;
		};

		/**
   * Represents a field of view defined by 4 different degree values describing the view from a center point.
   * @class VRFieldOfView
   */

		var VRFieldOfView = function (_VRFieldOfViewReadOnl) {
			_inherits(VRFieldOfView, _VRFieldOfViewReadOnl);

			function VRFieldOfView(up, right, down, left) {
				_classCallCheck(this, VRFieldOfView);

				return _possibleConstructorReturn(this, Object.getPrototypeOf(VRFieldOfView).call(this, up, right, down, left));
			}

			return VRFieldOfView;
		}(VRFieldOfViewReadOnly);

		/**
   * Provides access to all the information required to correctly render a scene for each given eye, including field of view information.
   * @class VREyeParameters
   */

		var VREyeParameters = function VREyeParameters() {
			_classCallCheck(this, VREyeParameters);

			this.minimumFieldOfView = 0;
			this.maximumFieldOfView = 0;
			this.recommendedFieldOfView = 0;
			this.eyeTranslation = 0;
			this.currentFieldOfView = 0;
			this.renderRect = {
				x: 0,
				y: 0,
				w: 0,
				h: 0
			};
		};

		/**
   * Represents the position state at a given timestamp (which includes orientation, position, velocity, and acceleration.)
   * @class VRPositionState
   */

		var VRPositionState = function VRPositionState() {
			_classCallCheck(this, VRPositionState);

			this.timeStamp = 0;
			this.hasPosition = false;
			this.position = {
				x: 0,
				y: 0,
				z: 0
			};
			this.linearVelocity = 0;
			this.linearAcceleration = 0;
			this.hasOrientation = false;
			this.orientation = {
				x: 0,
				y: 0,
				z: 0
			};
			this.angularVelocity = 0;
			this.angularAcceleration = 0;
		};

		/**
   * A generic VR device, includes information such as device IDs and descriptions. Inherited by HMDVRDevice and PositionSensorVRDevice.
   * @class VRDevice
   */

		var VRDevice = function VRDevice() {
			_classCallCheck(this, VRDevice);

			this.hardwareUnitId = -1;
			this.deviceId = generateUUID();
			this.deviceName = "VRDevice";
		};

		/**
   * Represents a head mounted display, providing access to information about each eye, and the current field of view.
   * @class HMDVRDevice
   */

		var HMDVRDevice = function (_VRDevice) {
			_inherits(HMDVRDevice, _VRDevice);

			function HMDVRDevice() {
				_classCallCheck(this, HMDVRDevice);

				var _this2 = _possibleConstructorReturn(this, Object.getPrototypeOf(HMDVRDevice).call(this));

				_this2._vrFieldOfView = new VRFieldOfView();
				return _this2;
			}

			_createClass(HMDVRDevice, [{
				key: "getEyeParameters",
				value: function getEyeParameters(eye) {
					return this._vrFieldOfView;
				}
			}, {
				key: "setFieldOfView",
				value: function setFieldOfView(leftFOV, rightFOV, zNear, zFar) {
					this._vrFieldOfView.leftDegrees = leftFOV;
					this._vrFieldOfView.rightDegrees = rightFOV;
					this._vrFieldOfView.downDegrees = zNear;
					this._vrFieldOfView.upDegrees = zFar;
				}
			}]);

			return HMDVRDevice;
		}(VRDevice);

		/**
   * Represents the position sensor for the VR hardware, allowing access to information such as position and orientation.
   * @class PositionSensorVRDevice
   */

		var PositionSensorVRDevice = function (_VRDevice2) {
			_inherits(PositionSensorVRDevice, _VRDevice2);

			function PositionSensorVRDevice() {
				_classCallCheck(this, PositionSensorVRDevice);

				var _this3 = _possibleConstructorReturn(this, Object.getPrototypeOf(PositionSensorVRDevice).call(this));

				_this3._positionState = new VRPositionState();
				return _this3;
			}

			_createClass(PositionSensorVRDevice, [{
				key: "getState",
				value: function getState() {
					return this._positionState;
				}
			}, {
				key: "getImmediateState",
				value: function getImmediateState() {
					return this._positionState;
				}
			}, {
				key: "resetSensor",
				value: function resetSensor() {}
			}]);

			return PositionSensorVRDevice;
		}(VRDevice);

		/**
   * Represents a head mounted display, providing access to information about each eye, and the current field of view.
   * @class MobileHMDVRDevice
   */

		var MobileHMDVRDevice = function (_HMDVRDevice) {
			_inherits(MobileHMDVRDevice, _HMDVRDevice);

			function MobileHMDVRDevice(guid) {
				_classCallCheck(this, MobileHMDVRDevice);

				var _this4 = _possibleConstructorReturn(this, Object.getPrototypeOf(MobileHMDVRDevice).call(this));

				_this4.hardwareUnitId = guid;
				_this4.deviceName = "WebVR Mobile HMD Device";
				return _this4;
			}

			return MobileHMDVRDevice;
		}(HMDVRDevice);

		/**
   * Represents the position sensor for the VR hardware, allowing access to information such as position and orientation.
   * @class MobilePositionSensorVRDevice
   */

		var MobilePositionSensorVRDevice = function (_PositionSensorVRDevi) {
			_inherits(MobilePositionSensorVRDevice, _PositionSensorVRDevi);

			function MobilePositionSensorVRDevice(guid) {
				_classCallCheck(this, MobilePositionSensorVRDevice);

				var _this5 = _possibleConstructorReturn(this, Object.getPrototypeOf(MobilePositionSensorVRDevice).call(this));

				_this5.hardwareUnitId = guid;
				_this5.deviceName = "WebVR Mobile Position Sensor Device";

				_this5._rotation = {
					x: 0,
					y: 0,
					z: 0
				};

				_this5._onOrientationChange = _this5._onOrientationChange.bind(_this5);
				return _this5;
			}

			// It's not good and need to be changed

			_createClass(MobilePositionSensorVRDevice, [{
				key: "_onOrientationChange",
				value: function _onOrientationChange(event) {
					var alpha = +event.alpha | 0;
					var beta = +event.beta | 0;
					var gamma = +event.gamma | 0;

					gamma = gamma < 0 ? 90 + gamma : 270 - gamma;

					this._positionState.orientation.x = gamma / 180.0 * Math.PI;
					this._positionState.orientation.y = -alpha / 180.0 * Math.PI;
					this._positionState.orientation.z = beta / 180.0 * Math.PI;
				}
			}]);

			return MobilePositionSensorVRDevice;
		}(PositionSensorVRDevice);

		// Normalize the JS engine.

		window.VREyeParameters = VREyeParameters;
		window.VRFieldOfViewReadOnly = VRFieldOfViewReadOnly;
		window.VRFieldOfView = VRFieldOfView;
		window.VRPositionState = VRPositionState;
		window.VRDevice = VRDevice;
		window.HMDVRDevice = HMDVRDevice;
		window.MobileHMDVRDevice = MobileHMDVRDevice;
		window.MobilePositionSensorVRDevice = MobilePositionSensorVRDevice;
		window.PositionSensorVRDevice = PositionSensorVRDevice;
	};

	// Checks if WebVR is here.
	if (!navigator.getVRDevices) {
		// Firefox has already all of that.
		if (!navigator.mozGetVRDevices) {
			__applyPolyfill();
		}

		navigator.getVRDevices = getVRDevices;
	}
})();