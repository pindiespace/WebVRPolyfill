# WebVRPolyfill

Normalize the WebVR API on desktop and mobile. The Desktop version does nothing except creating WebVR objects in the global scope. 
On mobile a `MobileHMDDevice` and `MobilePositionTrackerDevice` will be created and used with the orientation events to provide basic head tracking.

## Setup

The polyfill is written in Java Script 2015, you have to convert the code in `ES5` before use it, or using a compatible browser like Microsoft Edge.

The `dist` folder contains an ES5 version ready to use in all browsers.

```bash
npm install
npm run build
```

## License
This project uses the MIT licence. Please take a look at the `LICENSE` file for more informations.