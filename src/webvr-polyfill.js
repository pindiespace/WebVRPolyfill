/**
 * This polyfill will create the WebVR API on desktop and mobile. The Desktop version does nothing except creating WebVR object in the global scope.
 * The mobile version will uses the orientation events to provide a baasic head tracking.
 *
 * The API and docs come from Mozilla Developer Docs https://developer.mozilla.org/en-US/docs/Web/API/WebVR_API
 *
 * Licence: MIT
 * Author: Yannick Comte
 * Initial release: 01-20-2016
 */
(function() {
	// Generates an UUID.
	var generateUUID = function() {
		return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
	};

	/**
	 * Gets available VR devices.
	 * @method getVRDevices
	 * @return {Promise} Returns a promise that resolves to an array of objects representing any VR devices that are connected to the computer and compatible with the browser.
	 */
	var getVRDevices = function () {
		return new Promise(function(resolve, reject) {
			// Mobile (If any one has a better method...)
			if (navigator.userAgent.match(/android|iphone|windows phone|mobile/i)) {
				let guid = generateUUID();
				let devices = [];
				devices.push(new MobileHMDVRDevice(guid));
				devices.push(new MobilePositionSensorVRDevice(guid));
				resolve(devices);
			}
			// Firefox
			else if (navigator.mozGetVRDevices) {
				navigator.mozGetVRDevices(resolve);
			}
			// Other (Chrome has already the correct implementation.)
			else {
				reject();
			}
		});
	};

	var __applyPolyfill = (function() {
		/**
		 * Contains the raw definition for the degree value properties required to define a field of view. Inherited by VRFieldOfView.
		 * @class VRFieldOfViewReadOnly
		 */
		class VRFieldOfViewReadOnly {
			constructor(up = 0, right = 0, down = 0, left = 0) {
				this.upDegrees = up;
				this.rightDegrees = right;
				this.downDegrees = down;
				this.leftDegrees = left;
			}
		}

		/**
		 * Represents a field of view defined by 4 different degree values describing the view from a center point.
		 * @class VRFieldOfView
		 */
		class VRFieldOfView extends VRFieldOfViewReadOnly {
			constructor(up, right, down, left) {
				super(up, right, down, left);
			}
		}

		/**
		 * Provides access to all the information required to correctly render a scene for each given eye, including field of view information.
		 * @class VREyeParameters
		 */
		class VREyeParameters {
			constructor() {
				this.minimumFieldOfView = 0;
				this.maximumFieldOfView = 0;
				this.recommendedFieldOfView = 0;
				this.eyeTranslation = 0;
				this.currentFieldOfView = 0;
				this.renderRect = {
					x: 0,
					y: 0,
					w: 0,
					h: 0
				};
			}
		}

		/**
		 * Represents the position state at a given timestamp (which includes orientation, position, velocity, and acceleration.)
		 * @class VRPositionState
		 */
		class VRPositionState {
			constructor() {
				this.timeStamp = 0;
				this.hasPosition = false;
				this.position = {
					x: 0,
					y: 0,
					z: 0
				};
				this.linearVelocity = 0;
				this.linearAcceleration = 0
				this.hasOrientation = false;
				this.orientation = {
					x: 0,
					y: 0,
					z: 0
				};
				this.angularVelocity = 0;
				this.angularAcceleration = 0;
			}
		}

		/**
		 * A generic VR device, includes information such as device IDs and descriptions. Inherited by HMDVRDevice and PositionSensorVRDevice.
		 * @class VRDevice
		 */
		class VRDevice {
			constructor() {
				this.hardwareUnitId = -1;
				this.deviceId = generateUUID();
				this.deviceName = "VRDevice";
			}
		}

		/**
		 * Represents a head mounted display, providing access to information about each eye, and the current field of view.
		 * @class HMDVRDevice
		 */
		class HMDVRDevice extends VRDevice {
			constructor() {
				super();
				this._vrFieldOfView = new VRFieldOfView();
			}

			getEyeParameters(eye) {
				return this._vrFieldOfView;
			}

			setFieldOfView(leftFOV, rightFOV, zNear, zFar) {
				this._vrFieldOfView.leftDegrees = leftFOV;
				this._vrFieldOfView.rightDegrees = rightFOV;
				this._vrFieldOfView.downDegrees = zNear;
				this._vrFieldOfView.upDegrees = zFar;
			}
		}

		/**
		 * Represents the position sensor for the VR hardware, allowing access to information such as position and orientation.
		 * @class PositionSensorVRDevice
		 */
		class PositionSensorVRDevice extends VRDevice {
			constructor() {
				super();
				this._positionState = new VRPositionState();
			}

			getState() {
				return this._positionState;
			}

			getImmediateState() {
				return this._positionState;
			}

			resetSensor() {}
		}

		/**
		 * Represents a head mounted display, providing access to information about each eye, and the current field of view.
		 * @class MobileHMDVRDevice
		 */
		class MobileHMDVRDevice extends HMDVRDevice {
			constructor(guid) {
				super();
				this.hardwareUnitId = guid;
				this.deviceName = "WebVR Mobile HMD Device";
			}
		}

		/**
		 * Represents the position sensor for the VR hardware, allowing access to information such as position and orientation.
		 * @class MobilePositionSensorVRDevice
		 */
		class MobilePositionSensorVRDevice extends PositionSensorVRDevice {
			constructor(guid) {
				super();

				this.hardwareUnitId = guid;
				this.deviceName = "WebVR Mobile Position Sensor Device";

				this._rotation = {
					x: 0,
					y: 0,
					z: 0
				};

				this._onOrientationChange = this._onOrientationChange.bind(this);
			}

			// It's not good and need to be changed
			_onOrientationChange(event) {
				var alpha = +event.alpha | 0;
				var beta = +event.beta | 0;
				var gamma = +event.gamma | 0;

				gamma = (gamma < 0) ? 90 + gamma : 270 - gamma;

				this._positionState.orientation.x = gamma / 180.0 * Math.PI;
				this._positionState.orientation.y = -alpha / 180.0 * Math.PI;
				this._positionState.orientation.z = beta / 180.0 * Math.PI;
			}
		}

		// Normalize the JS engine.
		window.VREyeParameters = VREyeParameters;
		window.VRFieldOfViewReadOnly = VRFieldOfViewReadOnly;
		window.VRFieldOfView = VRFieldOfView;
		window.VRPositionState = VRPositionState;
		window.VRDevice = VRDevice;
		window.HMDVRDevice = HMDVRDevice;
		window.MobileHMDVRDevice = MobileHMDVRDevice;
		window.MobilePositionSensorVRDevice = MobilePositionSensorVRDevice;
		window.PositionSensorVRDevice = PositionSensorVRDevice;
	});

	// Checks if WebVR is here.
	if (!navigator.getVRDevices) {
		// Firefox has already all of that.
		if (!navigator.mozGetVRDevices) {
			__applyPolyfill();
		}

		navigator.getVRDevices = getVRDevices;
	}
})();